package de.mstock.monolith.config;


/**
 * All Routes of backend.
 */
public class RoutesConfig {
    public static String userManagementServer = "http://localhost:8083/";
    public static String login = userManagementServer + "login";
    public static String registerUser = userManagementServer + "user/register";
    public static String editUser = userManagementServer + "user/edit";
    public static String editPerson = userManagementServer + "person/edit";
    public static String createPerson = userManagementServer + "person/create";
    public static String getPersons = userManagementServer + "persons";
    public static String getUser = userManagementServer + "user";
}
