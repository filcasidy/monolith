package de.mstock.monolith.web;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.mstock.monolith.config.RoutesConfig;
import de.mstock.monolith.rest.RestTemplateFactory;
import de.mstock.monolith.service.SecurityService;
import de.mstock.monolith.web.form.PersonForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.ArrayList;

/**
 * Controller to edit the persons information.
 */
@Controller
public class EditPersonController extends WebMvcConfigurerAdapter {

    private static final String TEMPLATE = "edit-person";
    private static final String USERNAME = "username";
    private static final String NAME = "name";
    private static final String LASTNAME = "lastname";
    private static final String EMAIL = "email";
    private static final String PERSON = "person";
    private static final String ID = "id";
    private static final String ERROR_MESSAGES = "errorMessages";
    private static final String SUCCESS = "success";
    private static final String PERSON_FORM = "personForm";

    @Autowired
    RestTemplateFactory restTemplateFactory;

    @Autowired
    SecurityService securityService;

    private int PersonId = 0;

    @ModelAttribute(PERSON_FORM)
    public PersonForm newPersonForm() {
        return new PersonForm();
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/edit-person").setViewName(TEMPLATE);
    }

    /**
     * Edit person page.
     *
     * @param model         template model
     * @param errorMessages messages to display
     * @return the template's name
     */
    @GetMapping(value = "/edit-person")
    public String editPerson(Model model, @ModelAttribute(ERROR_MESSAGES) ArrayList<FieldError> errorMessages, PersonForm personForm) {
        RestTemplate restTemplate = restTemplateFactory.getObject();
        restTemplate.getInterceptors().clear();
        restTemplate.getInterceptors().add(securityService.getCurrentBasicAuthorizationInterceptor());

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(RoutesConfig.getUser)
                .queryParam(USERNAME, securityService.getUserAuthentication().getName());

        HttpEntity<?> entity = new HttpEntity<>(headers);

        HttpEntity<String> response = restTemplate.exchange(
                builder.build().encode().toUri(),
                HttpMethod.GET,
                entity,
                String.class);

        JsonElement result = new JsonParser().parse(response.getBody());
        JsonElement person = result.getAsJsonObject().get(PERSON);
        model.addAttribute(NAME, person.getAsJsonObject().get(NAME).getAsString());
        model.addAttribute(LASTNAME, person.getAsJsonObject().get(LASTNAME).getAsString());
        model.addAttribute(EMAIL, person.getAsJsonObject().get(EMAIL).getAsString());
        PersonId = person.getAsJsonObject().get(ID).getAsInt();

        personForm.setErrors(new ArrayList<>());
        for (FieldError fieldError : errorMessages) {
            personForm.getErrors().add(fieldError.getField() + ": " + fieldError.getDefaultMessage());
        }
        if (!errorMessages.isEmpty()) {
            model.addAttribute(SUCCESS, false);
        }
        return TEMPLATE;
    }

    /**
     * Edit person post.
     *
     * @return The template's name.
     */
    @PostMapping(value = "/edit-person")
    public String registerUser(@Valid @ModelAttribute(PERSON_FORM) PersonForm personForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(ERROR_MESSAGES, bindingResult.getFieldErrors());
            return "redirect:/" + TEMPLATE;
        }

        RestTemplate restTemplate = restTemplateFactory.getObject();
        restTemplate.getInterceptors().clear();
        restTemplate.getInterceptors().add(securityService.getCurrentBasicAuthorizationInterceptor());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        JsonObject map = new JsonObject();
        map.addProperty(ID, PersonId);
        map.addProperty(NAME, personForm.getName());
        map.addProperty(LASTNAME, personForm.getLastname());
        map.addProperty(EMAIL, personForm.getEmail());
        HttpEntity<String> request = new HttpEntity<>(map.toString(), headers);
        restTemplate.postForEntity(RoutesConfig.editPerson, request, String.class);
        redirectAttributes.addFlashAttribute("success", true);
        return "redirect:/account";
    }
}
