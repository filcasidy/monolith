package de.mstock.monolith.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Locale;

/**
 * Controller for account management.
 */
@Controller
public class AccountController {

    private static final String TEMPLATE = "account";

    /**
     * Homepage
     *
     * @param model  Template model
     * @param locale Current locale
     * @return The template's name.
     */
    @GetMapping(value = "/account")
    public String register(Model model, Locale locale) {
        return TEMPLATE;
    }
}
