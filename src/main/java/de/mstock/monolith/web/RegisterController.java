package de.mstock.monolith.web;

import de.mstock.monolith.config.RoutesConfig;
import de.mstock.monolith.web.form.RegistrationForm;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;

/**
 * Controller to register new users.
 */
@Controller
public class RegisterController extends WebMvcConfigurerAdapter {

    private static final String TEMPLATE = "register";
    private static final String LASTNAME = "lastname";
    private static final String EMAIL = "email";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String ERROR_MESSAGES = "errorMessages";
    private static final String REGISTRATION_FORM = "registrationForm";
    private static final String SUCCESS = "success";
    private static final String NAME = "name";
    private String temporaryName = "";
    private String temporaryLastname = "";
    private String temporaryUsername = "";
    private String temporaryEmail = "";

    @ModelAttribute(REGISTRATION_FORM)
    public RegistrationForm newRegistrationForm() {
        return new RegistrationForm();
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/register").setViewName(TEMPLATE);
    }

    /**
     * Registration page.
     *
     * @param model            template model
     * @param errorMessages    messages to display
     * @param registrationForm the form with errors
     * @return The template's name.
     */
    @GetMapping(value = "/register")
    public String register(Model model, RegistrationForm registrationForm, @ModelAttribute(ERROR_MESSAGES) ArrayList<FieldError> errorMessages) {
        registrationForm.setError(new ArrayList<>());
        for (FieldError fieldError : errorMessages) {
            registrationForm.getError().add(fieldError.getField() + ": " + fieldError.getDefaultMessage());
        }
        if (!errorMessages.isEmpty()) {
            model.addAttribute(SUCCESS, false);
        }
        model.addAttribute(NAME, temporaryName);
        model.addAttribute(LASTNAME, temporaryLastname);
        model.addAttribute(EMAIL, temporaryEmail);
        model.addAttribute(USERNAME, temporaryUsername);
        return TEMPLATE;
    }

    /**
     * Registration post.
     *
     * @return The template's name.
     */
    @PostMapping(value = "/register")
    public String registerUser(@Valid @ModelAttribute(REGISTRATION_FORM) RegistrationForm registrationForm, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        temporaryName = registrationForm.getName();
        temporaryLastname = registrationForm.getLastname();
        temporaryUsername = registrationForm.getUsername();
        temporaryEmail = registrationForm.getEmail();
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(ERROR_MESSAGES, bindingResult.getFieldErrors());
            return "redirect:/" + TEMPLATE;
        }
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add(NAME, registrationForm.getName());
        map.add(LASTNAME, registrationForm.getLastname());
        map.add(USERNAME, registrationForm.getUsername());
        map.add(PASSWORD, registrationForm.getPassword());
        map.add(EMAIL, registrationForm.getEmail());

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        restTemplate.postForEntity(RoutesConfig.registerUser, request, String.class);
        return "redirect:/";
    }
}
