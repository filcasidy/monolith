package de.mstock.monolith.web;

import java.util.Locale;

import de.mstock.monolith.service.SecurityService;
import de.mstock.monolith.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomepageController {

  private static final String TEMPLATE = "homepage";

  @Autowired
  private ShopService shopService;

  @Autowired
  private SecurityService securityService;

  /**
   * Homepage
   * 
   * @param model Template model
   * @param locale Current locale
   * @return The template's name.
   */
  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String homepage(Model model, Locale locale) {
    model.addAttribute("categories", shopService.getCategories(locale));
    model.addAttribute("userIsAuthenticated", securityService.userIsAuthenticated());
    return TEMPLATE;
  }
}
