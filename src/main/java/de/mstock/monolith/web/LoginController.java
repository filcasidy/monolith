package de.mstock.monolith.web;

import de.mstock.monolith.config.RoutesConfig;
import de.mstock.monolith.rest.RestTemplateFactory;
import de.mstock.monolith.service.SecurityService;
import de.mstock.monolith.web.form.LoginForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;

/**
 * Login and authentication controller.
 */
@Controller
public class LoginController {

    private static final String TEMPLATE = "login";

    @Autowired
    RestTemplateFactory restTemplateFactory;

    @Autowired
    SecurityService securityService;

    private String errorMessage = "";

    /**
     * Login request.
     *
     * @param model Template model
     * @return The template's name.
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {

        if (!errorMessage.isEmpty()) {
            model.addAttribute("success", false);
            model.addAttribute("errorMessage", errorMessage);
            errorMessage = "";
            return TEMPLATE;
        }

        if (securityService.userIsAuthenticated()) {
            //TODO Wenn der Nutzer bereits eingeloggt ist, muss er auf die User Dashboard Seite weitergeleitet werden
            return "redirect:/";
        } else {
            return TEMPLATE;
        }
    }

    /**
     * Logout request.
     *
     * @return redirect to homepage.
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout() {
        securityService.logoutUser();
        return "redirect:/";
    }

    /**
     * Authentication request.
     *
     * @return The template's name.
     */
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public String authenticate(@Valid LoginForm loginForm) {
        String username = loginForm.getUsername();
        String password = loginForm.getPassword();

        RestTemplate restTemplate = restTemplateFactory.getObject();
        restTemplate.getInterceptors().clear();
        restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(username, password));

        try {
            ResponseEntity<String> response = restTemplate.exchange(RoutesConfig.login, HttpMethod.GET, null, String.class);
            if (response.getStatusCode() == HttpStatus.ACCEPTED) {
                securityService.loginUser(username, password);
                return "redirect:/";
            }
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().is4xxClientError()) {
                errorMessage = e.getStatusCode().toString() + ": Bad credentials.";
                return "redirect:/" + TEMPLATE;
            } else {
                errorMessage = "Something went wrong, try again.";
                return "redirect:/" + TEMPLATE;
            }
        }
        return TEMPLATE;
    }
}
