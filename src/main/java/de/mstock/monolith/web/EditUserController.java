package de.mstock.monolith.web;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.mstock.monolith.config.RoutesConfig;
import de.mstock.monolith.rest.RestTemplateFactory;
import de.mstock.monolith.service.SecurityService;
import de.mstock.monolith.web.form.UserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.ArrayList;

/**
 * Controller to edit the users information.
 */
@Controller
public class EditUserController extends WebMvcConfigurerAdapter {

    private static final String OLD_USERNAME = "oldUsername";
    private static final String NEW_USERNAME = "newUsername";
    private static final String OLD_PASSWORD = "oldPassword";
    private static final String NEW_PASSWORD = "newPassword";
    private static final String TEMPLATE = "edit-user";
    private static final String USERNAME = "username";
    private static final String SUCCESS = "success";
    private static final String USER_FORM = "userForm";
    private static final String ERROR_MESSAGES = "errorMessages";
    @Autowired
    RestTemplateFactory restTemplateFactory;
    @Autowired
    SecurityService securityService;
    private ArrayList<String> redirectErrors = new ArrayList<>();

    @ModelAttribute(USER_FORM)
    public UserForm newUserForm() {
        return new UserForm();
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/edit-user").setViewName(TEMPLATE);
    }

    /**
     * Edit user page.
     *
     * @param model         template model
     * @param errorMessages messages to display
     * @return The template's name.
     */
    @GetMapping(value = "/edit-user")
    public String editUser(Model model, UserForm userForm, @ModelAttribute("errorMessages") ArrayList<FieldError> errorMessages) {
        HttpEntity<String> response = getUser(securityService.getUserAuthentication().getName());

        JsonElement result = new JsonParser().parse(response.getBody());
        model.addAttribute(USERNAME, result.getAsJsonObject().get(USERNAME).getAsString());

        userForm.setErrors(new ArrayList<>());
        for (FieldError fieldError : errorMessages) {
            userForm.getErrors().add(fieldError.getField() + ": " + fieldError.getDefaultMessage());
            model.addAttribute(SUCCESS, false);
        }
        if (!redirectErrors.isEmpty()) {
            redirectErrors.forEach(errorMessage -> userForm.getErrors().add(errorMessage));
            redirectErrors.clear();
            model.addAttribute(SUCCESS, false);
        }
        return TEMPLATE;
    }

    public HttpEntity<String> getUser(String username) {
        RestTemplate restTemplate = restTemplateFactory.getObject();
        restTemplate.getInterceptors().clear();
        restTemplate.getInterceptors().add(securityService.getCurrentBasicAuthorizationInterceptor());

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(RoutesConfig.getUser)
                .queryParam(USERNAME, username);

        HttpEntity<?> entity = new HttpEntity<>(headers);

        return restTemplate.exchange(
                builder.build().encode().toUri(),
                HttpMethod.GET,
                entity,
                String.class);
    }

    /**
     * Edit user post.
     *
     * @return The template's name.
     */
    @PostMapping(value = "/edit-user")
    public String editUser(@Valid @ModelAttribute(USER_FORM) UserForm userForm, Model model, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute(ERROR_MESSAGES, bindingResult.getFieldErrors());
            return "redirect:/" + TEMPLATE;
        }

        RestTemplate restTemplate = restTemplateFactory.getObject();
        restTemplate.getInterceptors().clear();
        restTemplate.getInterceptors().add(securityService.getCurrentBasicAuthorizationInterceptor());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        JsonObject map = new JsonObject();
        map.addProperty(OLD_USERNAME, securityService.getUserAuthentication().getName());
        map.addProperty(NEW_USERNAME, userForm.getUsername());
        map.addProperty(OLD_PASSWORD, userForm.getOldPassword());
        map.addProperty(NEW_PASSWORD, userForm.getNewPassword());

        HttpEntity<String> request = new HttpEntity<>(map.toString(), headers);
        ResponseEntity<String> responseEntity = null;
        try {
            responseEntity = restTemplate.postForEntity(RoutesConfig.editUser, request, String.class);
        } catch (HttpClientErrorException e) {
        }
        if (responseEntity == null) {
            redirectErrors.add("The old password is incorrect.");
            return "redirect:/" + TEMPLATE;
        }
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            securityService.logoutUser();
            redirectAttributes.addFlashAttribute("relog", true);
            return "redirect:/";
        }
        if (responseEntity.getStatusCode().is5xxServerError()) {
            model.addAttribute("success", false);
        }
        return "redirect:/" + TEMPLATE;
    }
}
