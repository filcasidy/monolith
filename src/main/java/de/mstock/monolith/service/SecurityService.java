package de.mstock.monolith.service;

import javax.servlet.http.HttpSession;

import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Service
public class SecurityService {

    final static String AUTHENTICATION_ID = "SPRING_SECURITY_AUTHENTICATION";

    public void loginUser(String username, String password) {
        Authentication authentication = new UsernamePasswordAuthenticationToken(username, password);
        getSession().setAttribute(AUTHENTICATION_ID, authentication);
    }

    public void logoutUser() {
        getSession().invalidate();
    }

    public BasicAuthorizationInterceptor getCurrentBasicAuthorizationInterceptor() throws AuthenticationException {
        String username = getUserAuthentication().getPrincipal().toString();
        String password = getUserAuthentication().getCredentials().toString();
        if (username.isEmpty() || password.isEmpty()) {
            throw new AuthenticationCredentialsNotFoundException("You must be logged in for this operation.");
        } else {
            return new BasicAuthorizationInterceptor(username, password);
        }
    }

    public boolean userIsAuthenticated() {
        Authentication authentication = (Authentication) getSession().getAttribute(AUTHENTICATION_ID);
        if (authentication != null) {
            return true;
        } else {
            return false;
        }
    }

    public Authentication getUserAuthentication() {
        return (Authentication) getSession().getAttribute(AUTHENTICATION_ID);
    }

    public String getSessionId() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attributes.getSessionId();
    }

    public HttpSession getSession() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true); // true == allow create
    }

}
