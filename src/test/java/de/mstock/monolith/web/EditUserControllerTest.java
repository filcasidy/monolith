package de.mstock.monolith.web;


import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslJsonBody;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;
import de.mstock.monolith.rest.RestTemplateFactory;
import de.mstock.monolith.service.SecurityService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EditUserControllerTest {

    @Rule
    public PactProviderRuleMk2 rule = new PactProviderRuleMk2("user_service_provider", "localhost", 8083, this);
    @InjectMocks
    private EditUserController editUserController;
    @Mock
    private SecurityService securityService;
    @Mock
    private RestTemplateFactory restTemplateFactory;

    @Before
    public void setUp() {
        when(restTemplateFactory.getObject()).thenReturn(new RestTemplate(new SimpleClientHttpRequestFactory()));
        when(securityService.getCurrentBasicAuthorizationInterceptor())
                .thenReturn(new BasicAuthorizationInterceptor("Username1", "Password"));
    }

    @Pact(provider = "user_service_provider", consumer = "user_service_consumer")
    public RequestResponsePact createFragment(PactDslWithProvider builder) {

        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", "application/json");

        PactDslJsonBody body = new PactDslJsonBody()
                .id()
                .stringType("username")
                .stringType("password")
                .object("person")
                .id()
                .stringType("name", "lastname", "email")
                .closeObject()
                .asBody();

        return builder
                .uponReceiving("ExampleJavaConsumerPactTest test interaction")
                .path("/user")
                .query("username=Username1")
                .method("GET")
                .headers(headers)
                .willRespondWith()
                .status(200)
                .headers(headers)
                .body(body)
                .toPact();
    }

    @Test
    @PactVerification("user_service_provider")
    public void shouldGetUser() throws IOException {
        editUserController.getUser("Username1");
    }
}
